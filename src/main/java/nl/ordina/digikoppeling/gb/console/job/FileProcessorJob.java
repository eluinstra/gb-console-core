/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.console.job;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import nl.clockwork.ebms.common.JAXBParser;
import nl.clockwork.ebms.model.EbMSDataSource;
import nl.clockwork.ebms.model.EbMSMessageContent;
import nl.clockwork.ebms.model.EbMSMessageContext;
import nl.clockwork.ebms.model.Role;
import nl.clockwork.ebms.service.EbMSMessageService;
import nl.logius.digikoppeling.gb._2017._04.DataReferenceRequest;
import nl.ordina.digikoppeling.gb.model.send.Compression;
import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.model.send.StatusInfo;
import nl.ordina.digikoppeling.gb.service.GBService;
import nl.ordina.upload._1.UploadExportBestand;
import nl.ordina.upload._1.UploadExportBestandBevestiging;
import nl.ordina.digikoppeling.gb.console.Utils;
import nl.ordina.digikoppeling.gb.console.dao.GBDAO;
import nl.ordina.digikoppeling.gb.console.model.FileTransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.xml.sax.SAXException;

public class FileProcessorJob implements InitializingBean, Job
{
	protected transient Log logger = LogFactory.getLog(getClass());
	private GBDAO gbDAO;
	private GBService gbClient;
	private Path fileShare;
	private String uploadUrl;
	private long maxSize;
	private Compression compression;
	private EbMSMessageService ebMSMessageClient;
	private String tenantId;
	private String cpaId;
	private String partyId;
	private String role;
	private String service;
	private Path processedFileShare;
	
	@Override
	public void afterPropertiesSet() throws Exception
	{
		createDirectory(fileShare);
		processedFileShare = fileShare.resolve("processed");
		createDirectory(processedFileShare);
	}

	private void createDirectory(Path fileShare) throws IOException
	{
		if (!Files.exists(fileShare))
			Files.createDirectory(fileShare);
	}

	@Override
	public void execute()
	{
		try
		{
			while (true)
				if (!handleNextBevestigenUploadExportBestand())
					if (!handleNextTransaction())
						if (!handleNextUnfinishedTransaction())
							if (!handleNextFile())
								break;
		}
		catch (IOException | InterruptedException e)
		{
			logger.error("",e);
		}
	}

	private boolean handleNextBevestigenUploadExportBestand() throws IOException
	{
		boolean result = false;
		EbMSMessageContext ebMSMessageContext = createMessageContextBevestigenUploadExportBestand();
		List<String> messageIds = ebMSMessageClient.getMessageIds(ebMSMessageContext,1);
		if (messageIds != null && messageIds.size() > 0)
		{
			result = true;
			String messageId = messageIds.get(0);
			logger.info("Executing handleNextBevestigenUploadExportBestand for message " + messageId);
			EbMSMessageContent message = ebMSMessageClient.getMessage(messageId,null);
			int nrAttachments = message.getDataSources().size();
			if (nrAttachments > 1)
				logger.warn("Message " + messageId + " invalid! 1 attachment expected, " + nrAttachments + " found.");
			EbMSDataSource ebMSDataSource = message.getDataSources().get(0);
			String contentType = ebMSDataSource.getContentType();
			if (contentType.matches("(application|text)/xml"))
			{
				try
				{
					String content = new String(ebMSDataSource.getContent(),"UTF8");
					validateXML("/nl/ordina/digikoppeling/gb/console/xsd/upload-1.0.xsd",content);
					UploadExportBestandBevestiging uploadExportBestandBevestiging = JAXBParser.getInstance(UploadExportBestandBevestiging.class).handle(content,UploadExportBestandBevestiging.class);
					String transactionId = uploadExportBestandBevestiging.getConversationId();
					if (!isValidUploadExportBestandBevestiging(uploadExportBestandBevestiging))
					{
						//TODO db transaction
						gbClient.resendTransaction(transactionId,uploadExportBestandBevestiging.getBestand());
						gbDAO.updateTransactionStatus(transactionId,Status.PENDING,null);
					}
					else
						logger.info("Transaction " + transactionId + " finished successfully.");
				}
				catch (SAXException | JAXBException | ValidationException e)
				{
					logger.error("Error processing message " + messageId,e);
				}
			}
			else
				logger.warn("Message " + messageId + " invalid! Content-Type application/xml or text/xml excpected, " + contentType + " found.");
			ebMSMessageClient.processMessage(messageId);
		}
		return result;
	}

	private void validateXML(final String xsdFile, final String content) throws SAXException, IOException
	{
		final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    final String systemId = this.getClass().getResource(xsdFile).toString();
    final Schema schema = factory.newSchema(new StreamSource(this.getClass().getResourceAsStream(xsdFile),systemId));
		final Validator validator = schema.newValidator();
		validator.validate(new StreamSource(new StringReader(content)));
	}

	private boolean isValidUploadExportBestandBevestiging(UploadExportBestandBevestiging uploadExportBestandBevestiging) throws ValidationException
	{
		if (!tenantId.equals(uploadExportBestandBevestiging.getTenantId()))
			throw new ValidationException("Expected tenantId " + tenantId + ", found " + uploadExportBestandBevestiging.getTenantId());
		if (!gbDAO.exists(uploadExportBestandBevestiging.getConversationId(),uploadExportBestandBevestiging.getBestand().getContent().getFilename()))
			throw new ValidationException("Transaction for file " + uploadExportBestandBevestiging.getBestand().getContent().getFilename() + " and transactionId " + uploadExportBestandBevestiging.getConversationId() + " does not exist!");
		return nl.logius.digikoppeling.gb._2017._04.Status.OK.equals(uploadExportBestandBevestiging.getBestand().getContent().getStatus());
	}

	private boolean handleNextTransaction()
	{
		boolean result = false;
		FileTransaction transaction = gbDAO.getNextSucceddedTransaction();
		if (transaction != null)
		{
			result = true;
			String transactionId = transaction.getId();
			logger.info("Executing handleNextTransaction for transaction " + transactionId);
			try
			{
				DataReferenceRequest dataReferenceRequest = gbClient.getDataReferenceRequest(transactionId);
				EbMSMessageContext ebMSMessageContext = createMessageContextUploadExportBestand();
				EbMSMessageContent ebMSMessageContent = new EbMSMessageContent();
				ebMSMessageContent.setContext(ebMSMessageContext);
				ArrayList<EbMSDataSource> dataSources = new ArrayList<EbMSDataSource>();
				UploadExportBestand uploadExportBestand = new UploadExportBestand();
				uploadExportBestand.setTenantId(tenantId);
				uploadExportBestand.setConversationId(transactionId);
				uploadExportBestand.setBestand(dataReferenceRequest);
				String xml = JAXBParser.getInstance(UploadExportBestand.class).handle(new JAXBElement<UploadExportBestand>(new QName("http://upload.ordina.nl/1.0","uploadExportBestand"),UploadExportBestand.class,uploadExportBestand));
				dataSources.add(new EbMSDataSource("uploadExportBestand.xml","application/xml",xml.getBytes("UTF8")));
				ebMSMessageContent.setDataSources(dataSources);
				ebMSMessageClient.sendMessage(ebMSMessageContent);
				gbDAO.processTransaction(transactionId);
			}
			catch (UnsupportedEncodingException | JAXBException e)
			{
				logger.error("Error processing transaction " + transactionId,e);
			}
		}
		return result;
	}

	private boolean handleNextUnfinishedTransaction() throws InterruptedException, IOException
	{
		boolean result = false;
		FileTransaction transaction = gbDAO.getNextUnfinishedTransaction();
		if (transaction != null)
		{
			result = true;
			String transactionId = transaction.getId();
			logger.info("Executing handleNextUnfinishedTransaction for transaction " + transactionId);
			logger.info("Transaction " + transactionId + " " + transaction.getStatus());
			StatusInfo transactionStatus = null;
			for (;(transactionStatus = gbClient.getStatus(transactionId)) != null && transactionStatus.getStatus().ordinal() <= Status.READY_TO_SEND.ordinal();)
				Thread.sleep(300000);
			//TODO db transaction???
			Path sourcePath = fileShare.resolve(transaction.getFilename());
			Path destinationPath = processedFileShare.resolve(transaction.getFilename());
			logger.info("Moving " + sourcePath.toString() + " to " + destinationPath.toString());
			if (Files.exists(sourcePath))
				Files.move(sourcePath,destinationPath,StandardCopyOption.REPLACE_EXISTING);
			gbDAO.updateTransactionStatus(transactionId,transactionStatus.getStatus(),transactionStatus.getMessage());
			logger.info("Transaction " + transactionId + " " + transactionStatus.getStatus());
		}
		return result;
	}

	private boolean handleNextFile() throws IOException
	{
		boolean result = false;
		if (Files.isDirectory(fileShare))
			try (DirectoryStream<Path> stream = Files.newDirectoryStream(fileShare))
			{
				for (Path file : stream)
					if (Files.isRegularFile(file))
					{
						String filename = file.getFileName().toString();
						if (!gbDAO.exists(filename))
						{
							result = true;
							logger.info("Executing handleNextFile for file " + filename);
							//TODO db transaction
							String transactionId = gbClient.sendFile(filename,Utils.getContentType(filename),uploadUrl,maxSize,compression);
							gbDAO.insertTransaction(transactionId,filename,Status.PENDING);
							logger.info("File " + filename + " sent in transaction " + transactionId);
							break;
						}
						else
							logger.warn("Transaction for file " + filename + " already exists!");
					}
			}
		else
			logger.error("File " + fileShare.toString() + " is not a valid directory!");
		return result;
	}

	private EbMSMessageContext createMessageContextUploadExportBestand()
	{
		EbMSMessageContext ebMSMessageContext = new EbMSMessageContext();
		ebMSMessageContext.setCpaId(cpaId);
		Role fromRole = new Role();
		fromRole.setPartyId(partyId);
		fromRole.setRole(role);
		ebMSMessageContext.setFromRole(fromRole);
		ebMSMessageContext.setService(service);
		ebMSMessageContext.setAction("uploadExportBestand");
		return ebMSMessageContext;
	}
	
	private EbMSMessageContext createMessageContextBevestigenUploadExportBestand()
	{
		EbMSMessageContext ebMSMessageContext = new EbMSMessageContext();
		ebMSMessageContext.setCpaId(cpaId);
		Role toRole = new Role();
		toRole.setPartyId(partyId);
		toRole.setRole(role);
		ebMSMessageContext.setToRole(toRole);
		ebMSMessageContext.setService(service);
		ebMSMessageContext.setAction("bevestigenUploadExportBestand");
		return ebMSMessageContext;
	}
	
	public void setGbDAO(GBDAO gbDAO)
	{
		this.gbDAO = gbDAO;
	}

	public void setGbClient(GBService gbClient)
	{
		this.gbClient = gbClient;
	}

	public void setFileShare(String fileShare)
	{
		this.fileShare = Paths.get(fileShare);
	}

	public void setUploadUrl(String uploadUrl)
	{
		this.uploadUrl = uploadUrl;
	}

	public void setMaxSize(long maxSize)
	{
		this.maxSize = maxSize;
	}

	public void setCompression(Compression compression)
	{
		this.compression = compression;
	}

	public void setEbMSMessageClient(EbMSMessageService ebMSMessageClient)
	{
		this.ebMSMessageClient = ebMSMessageClient;
	}

	public void setTenantId(String tenantId)
	{
		this.tenantId = tenantId;
	}

	public void setCpaId(String cpaId)
	{
		this.cpaId = cpaId;
	}

	public void setPartyId(String partyId)
	{
		this.partyId = partyId;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

	public void setService(String service)
	{
		this.service = service;
	}

}
