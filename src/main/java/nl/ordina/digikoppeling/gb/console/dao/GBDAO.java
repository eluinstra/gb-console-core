/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.console.dao;

import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.console.model.FileTransaction;

public interface GBDAO
{

	void executeTransaction(DAOTransactionCallback callback) throws DAOException;

	FileTransaction getNextUnfinishedTransaction();
	FileTransaction getNextSucceddedTransaction();

	boolean exists(String filename);
	boolean exists(String id, String filename);

	void insertTransaction(String id, String filename, Status status);

	void updateTransactionStatus(String id, Status status, String message);

	void processTransaction(String id);

}
