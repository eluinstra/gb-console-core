/**
 * Copyright 2019 Ordina
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ordina.digikoppeling.gb.console.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import nl.clockwork.ebms.dao.DAOException;
import nl.ordina.digikoppeling.gb.model.send.Status;
import nl.ordina.digikoppeling.gb.console.model.FileTransaction;

public class GBDAOImpl implements GBDAO
{
	private class FileTransactionRowMapper implements RowMapper<FileTransaction>
	{

		@Override
		public FileTransaction mapRow(ResultSet rs, int rownr) throws SQLException
		{
			FileTransaction result = new FileTransaction();
			result.setId(rs.getString("id"));
			result.setFilename(rs.getString("filename"));
			result.setStatus(Status.values()[rs.getInt("status")]);
			return result;
		}
	}

	protected TransactionTemplate transactionTemplate;
	protected JdbcTemplate jdbcTemplate;
	
	public GBDAOImpl(TransactionTemplate transactionTemplate, JdbcTemplate jdbcTemplate)
	{
		this.transactionTemplate = transactionTemplate;
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void executeTransaction(final DAOTransactionCallback callback) throws DAOException
	{
		try
		{
			transactionTemplate.execute(
				new TransactionCallbackWithoutResult()
				{

					@Override
					protected void doInTransactionWithoutResult(TransactionStatus transactionStatus)
					{
						callback.doInTransaction();
					}
				}
			);
		}
		catch (DataAccessException | TransactionException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public FileTransaction getNextUnfinishedTransaction()
	{
		List<FileTransaction> transactions = jdbcTemplate.query(new PreparedStatementCreator()
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection c) throws SQLException
			{
				PreparedStatement ps = c.prepareStatement(
					"select id, filename, status from file_transaction where status <= ? and processed = 0 order by timestamp asc"
				);
				ps.setInt(1,Status.READY_TO_SEND.ordinal());
				return ps;
			}
		},
		new FileTransactionRowMapper());
		if (transactions.size() > 0)
			return transactions.get(0);
		else
			return null;
	}

	@Override
	public FileTransaction getNextSucceddedTransaction()
	{
		List<FileTransaction> transactions = jdbcTemplate.query(new PreparedStatementCreator()
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection c) throws SQLException
			{
				PreparedStatement ps = c.prepareStatement(
					"select id, filename, status from file_transaction where status = ? and processed = 0 order by timestamp asc"
				);
				ps.setInt(1,Status.SUCCEEDED.ordinal());
				return ps;
			}
		},
		new FileTransactionRowMapper());
		if (transactions.size() > 0)
			return transactions.get(0);
		else
			return null;
	}

	@Override
	public boolean exists(String filename)
	{
		try
		{
			return jdbcTemplate.queryForObject(
				"select count(*)" +
				" from file_transaction" +
				" where filename = ?",
				Integer.class,
				filename
			) > 0;
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public boolean exists(String id, String filename)
	{
		try
		{
			return jdbcTemplate.queryForObject(
				"select count(*)" +
				" from file_transaction" +
				" where id = ?" +
				" and filename = ?",
				Integer.class,
				id,
				filename
			) > 0;
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void insertTransaction(String id, String filename, Status status)
	{
		try
		{
			Date now = new Date();
			jdbcTemplate.update
			(
				"insert into file_transaction (id,timestamp,filename,status,status_time,processed) values (?,?,?,?,?,?)",
				id,
				now,
				filename,
				status.ordinal(),
				now,
				0
			);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void updateTransactionStatus(String id, Status status, String message)
	{
		try
		{
			jdbcTemplate.update("update file_transaction set status = ?, status_time = ?, status_message = ?, processed = 0 where id = ?",status.ordinal(),new Date(),message,id);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

	@Override
	public void processTransaction(String id)
	{
		try
		{
			jdbcTemplate.update("update file_transaction set processed = 1 where id = ?",id);
		}
		catch (DataAccessException e)
		{
			throw new DAOException(e);
		}
	}

}
